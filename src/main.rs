extern crate universe;
extern crate gmp;
extern crate rust_mpfr;
use std::io::{stdin, stdout, Write};
use std::str::FromStr;
use std::convert::{From, Into};
use gmp::mpz::Mpz;
use rust_mpfr::mpfr::Mpfr;
use universe::universe::{Universe, Region, PARSEC_LS, METER};
use universe::data::octree::*;
use universe::data::vector::HasCoordinates3;

fn input(text: &str, buf: &mut String) {
	print!("{}", text);
	stdout().flush();
	stdin().read_line(buf);
}

fn main() {
	// c_ulong is 32-bit on windows :(
	let pc = PARSEC_LS as i64;
	let ls = 299792458i64;
	let mln = 1000000;
	let m = METER as i64;

	print!("Input seed for the universe (any string): ");
	stdout().flush();
	let mut seed = String::new();
	stdin().read_line(&mut seed);

	let universe = Universe::new(&seed.replace("\n","").replace("\r",""), 106, Box::new(|_, _, _| 1.0));
	let mut zip = universe.galaxy_tree().as_zipper();
	
	if let Octree::Node { ref data, .. } = *zip {
		println!("Seed: {:x}", universe.seed);
		println!("Number of galaxies: {}", data.num_galaxies.to_string());
	}
	else {
		panic!("ERROR: Main octree node isn't a Node!");
	}
	
	let mut buf = String::new();
	println!("\nInput the coordinates for the block (in megaparsecs):");
	input("Corner 1 X: ", &mut buf);
	let c1x = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc1x = Mpz::from(c1x) * mln * pc * ls * m;
	buf.clear();
	input("Corner 1 Y: ", &mut buf);
	let c1y = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc1y = Mpz::from(c1y) * mln * pc * ls * m;
	buf.clear();
	input("Corner 1 Z: ", &mut buf);
	let c1z = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc1z = Mpz::from(c1z) * mln * pc * ls * m;
	buf.clear();
	input("Corner 2 X: ", &mut buf);
	let c2x = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc2x = Mpz::from(c2x) * mln * pc * ls * m;
	buf.clear();
	input("Corner 2 Y: ", &mut buf);
	let c2y = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc2y = Mpz::from(c2y) * mln * pc * ls * m;
	buf.clear();
	input("Corner 2 Z: ", &mut buf);
	let c2z = i64::from_str(&buf.replace("\n","").replace("\r","")).unwrap();
	let mc2z = Mpz::from(c2z) * mln * pc * ls * m;
	buf.clear();

	let block = Region::new(106, mc1x, mc1y, mc1z, mc2x, mc2y, mc2z);
	
	let galaxies = zip.galaxies_in_block(&block);
	
	println!("\nGalaxies in the block: {}\n", galaxies.len());
	
	for i in 0..galaxies.len() {
		print!("Galaxy #{}: ", i+1);
		let galaxy = &galaxies[i];
		let x: &Mpz = galaxy.x();
		let y: &Mpz = galaxy.y();
		let z: &Mpz = galaxy.z();
		let x = Mpfr::from(x.clone()) / mln as f64 / pc as f64 / ls as f64 / m as f64;
		let y = Mpfr::from(y.clone()) / mln as f64 / pc as f64 / ls as f64 / m as f64;
		let z = Mpfr::from(z.clone()) / mln as f64 / pc as f64 / ls as f64 / m as f64;
		println!("({}, {}, {})", Into::<f64>::into(&x), Into::<f64>::into(&y), Into::<f64>::into(&z));
	}
}
